#include "image.h"

struct image create_image(uint32_t width, uint32_t height){
    if(height * width == 0) {
        error_actions(IMAGE_STRUCT_BROKEN, NULL, NULL);
        return (struct image){width, height, NULL};
    }
    struct pixel *ptr = malloc(width * height * (uint32_t)sizeof(struct pixel));
    if(ptr == NULL) {
        error_actions(NOT_ENOUGH_MEMORY_IN_HEAP, NULL, NULL);
        return (struct image){width, height, NULL};
    }

    return (struct image) {
        .width = width,
        .height = height,
        .data = ptr
    };
}

void clear_image(struct image *img){
    free(img->data);
    img->data = NULL;
}
